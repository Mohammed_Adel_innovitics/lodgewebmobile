import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddlodgeComponent } from './addlodge.component';

describe('AddlodgeComponent', () => {
  let component: AddlodgeComponent;
  let fixture: ComponentFixture<AddlodgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddlodgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddlodgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
