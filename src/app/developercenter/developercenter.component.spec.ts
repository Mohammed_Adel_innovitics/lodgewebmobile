import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopercenterComponent } from './developercenter.component';

describe('DevelopercenterComponent', () => {
  let component: DevelopercenterComponent;
  let fixture: ComponentFixture<DevelopercenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopercenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopercenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
