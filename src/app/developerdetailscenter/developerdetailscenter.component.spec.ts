import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeveloperdetailscenterComponent } from './developerdetailscenter.component';

describe('DeveloperdetailscenterComponent', () => {
  let component: DeveloperdetailscenterComponent;
  let fixture: ComponentFixture<DeveloperdetailscenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeveloperdetailscenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeveloperdetailscenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
