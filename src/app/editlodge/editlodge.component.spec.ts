import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditlodgeComponent } from './editlodge.component';

describe('EditlodgeComponent', () => {
  let component: EditlodgeComponent;
  let fixture: ComponentFixture<EditlodgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditlodgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditlodgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
