import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editlodge',
  templateUrl: './editlodge.component.html',
  styleUrls: ['./editlodge.component.css']
})
export class EditlodgeComponent implements OnInit {
  public first = false ;
  public second = false;
  public third = false;

  public addRequest = {
    'process' : '',
  }
  public count : any ;
  constructor() { }

  ngOnInit() {
  //this.selected = 's1';
  	this.first = true ;
    this.addRequest.process = 'S';
    this.count = 1 ;
  }
  toggle(type : any , operation : any) {
  console.log(this.addRequest.process);
  (operation == '+') ? this.count = this.count + 1 : this.count = this.count - 1
    switch(type){

        case 'first' :
        this.first = true ;
        this.second = false;
        this.third = false ;
        break;

        case 'second' :
        this.first = false ;
        this.second = true;
        this.third = false ;
        break;

        case 'third' :
        this.first = false ;
        this.second = false;
        this.third = true ;
        break;

        default :
        break;

        }
    }

}
