import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoritdetailsComponent } from './favoritdetails.component';

describe('FavoritdetailsComponent', () => {
  let component: FavoritdetailsComponent;
  let fixture: ComponentFixture<FavoritdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoritdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
