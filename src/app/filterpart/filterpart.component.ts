import { Component, OnInit } from '@angular/core';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-filterpart',
  templateUrl: './filterpart.component.html',
  styleUrls: ['./filterpart.component.css']
})
export class FilterpartComponent implements OnInit {
	value1: number = 2;
  highValue1: number = 7;
  options1: Options = {
    floor: 1,
    ceil: 12
  };


  value2: number = 2;
  highValue2: number = 3;
    options2: Options = {
    floor: 1,
    ceil: 5
  };

  value3: number = 218;
  highValue3: number = 360;
  options3: Options = {
    floor: 200,
    ceil: 400
  };





  constructor() { }

  ngOnInit() {
  }

}
