import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindlodgeComponent } from './findlodge.component';

describe('FindlodgeComponent', () => {
  let component: FindlodgeComponent;
  let fixture: ComponentFixture<FindlodgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindlodgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindlodgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
