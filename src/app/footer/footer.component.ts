import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public langSelector : any ;
  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this.langSelector = 'E';
    this.translate.use('en');
  }

  switchLang(lang : any){
    if(lang == 'E'){
      this.translate.use('en');
    }else{
      this.translate.use('ar');
    }
  }

}
