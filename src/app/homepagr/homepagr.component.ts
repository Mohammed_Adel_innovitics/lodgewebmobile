import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepagr',
  templateUrl: './homepagr.component.html',
  styleUrls: ['./homepagr.component.css']
})
export class HomepagrComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  slides = [
    {img: "./assets/images/october6.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"}

  ];
 
  slideConfig = {
    "slidesToShow": 2.5, 
    "slidesToScroll": 2,
    "nextArrow":"<div class='nav-btn next-slide'></div>",
    "prevArrow":"<div class='nav-btn prev-slide'></div>",
    "dots":true,
    "infinite": false
  };
  
  addSlide() {
    this.slides.push({img: "http://placehold.it/350x150/777777"})
  }
  
  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }
  
  slickInit(e) {
    console.log('slick initialized');
  }
  
  breakpoint(e) {
    console.log('breakpoint');
  }
  
  afterChange(e) {
    console.log('afterChange');
  }
  
  beforeChange(e) {
    console.log('beforeChange');
  }



  slides2 = [
    {img: "./assets/images/october6.jpg"},
    {img: "./assets/images/nourth.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"}

  ];
 
  slideConfig2 = {
    "slidesToShow": 2.5, 
    "slidesToScroll": 2,
    "nextArrow":"<div class='nav-btn next-slide'></div>",
    "prevArrow":"<div class='nav-btn prev-slide'></div>",
    "dots":true,
    "infinite": false
  };

  slickInit2(e) {
    console.log('slick initialized');
  }



  slides3 = [
    {img: "./assets/images/october6.jpg"},
    {img: "./assets/images/nourth.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"},
    {img: "./assets/images/Lead-Image-IKEA-770x470.jpg"}

  ];
 
  slideConfig3 = {
    "slidesToShow": 2.5, 
    "slidesToScroll": 2,
    "nextArrow":"<div class='nav-btn next-slide'></div>",
    "prevArrow":"<div class='nav-btn prev-slide'></div>",
    "dots":true,
    "infinite": false
  };

  slickInit3(e) {
    console.log('slick initialized');
  }

}
