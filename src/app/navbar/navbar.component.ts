import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public langSelector : any = true ;
  

  constructor(private translate: TranslateService) { }

  ngOnInit() {
  var body = document.getElementsByTagName("body")[0];
          var att = document.createAttribute("class");       // Create a "class" attribute
          att.value = "font-en";
          body.setAttributeNode(att);
    this.translate.use('en');
  }

  switchLang(lang : any){
    if(lang){
      this.translate.use('en');
      var body = document.getElementsByTagName("body")[0];
          var att = document.createAttribute("class");       // Create a "class" attribute
          att.value = "font-en";
          body.setAttributeNode(att);
    }else{
      this.translate.use('ar');
      var body = document.getElementsByTagName("body")[0];
          var att = document.createAttribute("class");       // Create a "class" attribute
          att.value = "font-ar";
          body.setAttributeNode(att);
    }
    this.langSelector = !lang;
  }



}
