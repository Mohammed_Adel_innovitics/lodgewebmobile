import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupenterverificationComponent } from './popupenterverification.component';

describe('PopupenterverificationComponent', () => {
  let component: PopupenterverificationComponent;
  let fixture: ComponentFixture<PopupenterverificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupenterverificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupenterverificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
