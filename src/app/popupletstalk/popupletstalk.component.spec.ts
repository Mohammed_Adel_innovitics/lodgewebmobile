import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupletstalkComponent } from './popupletstalk.component';

describe('PopupletstalkComponent', () => {
  let component: PopupletstalkComponent;
  let fixture: ComponentFixture<PopupletstalkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupletstalkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupletstalkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
