import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupnolodgeComponent } from './popupnolodge.component';

describe('PopupnolodgeComponent', () => {
  let component: PopupnolodgeComponent;
  let fixture: ComponentFixture<PopupnolodgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupnolodgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupnolodgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
