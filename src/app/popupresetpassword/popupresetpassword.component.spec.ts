import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupresetpasswordComponent } from './popupresetpassword.component';

describe('PopupresetpasswordComponent', () => {
  let component: PopupresetpasswordComponent;
  let fixture: ComponentFixture<PopupresetpasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupresetpasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupresetpasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
