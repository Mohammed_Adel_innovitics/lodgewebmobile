import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupsentsuccessComponent } from './popupsentsuccess.component';

describe('PopupsentsuccessComponent', () => {
  let component: PopupsentsuccessComponent;
  let fixture: ComponentFixture<PopupsentsuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupsentsuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupsentsuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
